import 'package:flutter/material.dart';
import 'package:test_flutter_web/admin_panel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: AdminPanel(),
    );
  }
}
