import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_flutter_web/admin_panel.dart';

part 'navigator_event.dart';

part 'navigator_state.dart';

class NavigatorBloc extends Bloc<NavigatorEvent, NavigatorState> {
  NavigatorBloc() : super(HomeNavigatorState());

  @override
  Stream<NavigatorState> mapEventToState(NavigatorEvent event) async* {
    if(event is ChangeScreenNavigatorEvent){
      yield* _mapScreen(event);
    }
  }

  Stream<NavigatorState> _mapScreen(ChangeScreenNavigatorEvent event) async*{
    switch(event.tab){
      case SideBarTab.Home:
        yield HomeNavigatorState();
        break;
      case SideBarTab.Grid:
        yield GridNavigatorState();
        break;
      case SideBarTab.List:
        yield ListNavigatorState();
        break;
    }
  }
}
