part of 'navigator_bloc.dart';

abstract class NavigatorState extends Equatable {
  @override
  List<Object> get props => [];
}

class HomeNavigatorState extends NavigatorState {}

class ListNavigatorState extends NavigatorState {}

class GridNavigatorState extends NavigatorState {}
