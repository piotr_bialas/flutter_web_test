part of 'navigator_bloc.dart';

abstract class NavigatorEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ChangeScreenNavigatorEvent extends NavigatorEvent{
  final SideBarTab tab;

  ChangeScreenNavigatorEvent(this.tab);

  @override
  List<Object> get props => [tab];
}
