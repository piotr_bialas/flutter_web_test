import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_admin_scaffold/admin_scaffold.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_shortcuts/keyboard_shortcuts.dart';
import 'package:test_flutter_web/bloc/navigator_bloc.dart' as nav;
import 'package:test_flutter_web/widgets/grid_widget.dart';
import 'package:test_flutter_web/widgets/home_widget.dart';
import 'package:test_flutter_web/widgets/list_widget.dart';

class AdminPanel extends StatefulWidget {
  AdminPanel({Key key}) : super(key: key);

  @override
  _AdminPanelState createState() => _AdminPanelState();
}

class _AdminPanelState extends State<AdminPanel> {
  nav.NavigatorBloc _navigatorBloc;
  String _selectedTab;

  @override
  void initState() {
    _navigatorBloc = nav.NavigatorBloc();
    _selectedTab = EnumToString.convertToString(SideBarTab.Home, camelCase: true);
    super.initState();
  }

  @override
  void dispose() {
    _navigatorBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyBoardShortcuts(
      keysToPress: {LogicalKeyboardKey.shiftLeft, LogicalKeyboardKey.keyD},
      onKeysPressed: () {},
      child: AdminScaffold(
        body: Scaffold(
          body: BlocBuilder(
            cubit: _navigatorBloc,
            builder: (BuildContext context, nav.NavigatorState state) {
              if (state is nav.HomeNavigatorState) {
                return HomeWidget();
              } else if (state is nav.GridNavigatorState) {
                return GridWidget();
              } else if (state is nav.ListNavigatorState) {
                return ListWidget();
              }
              return Container();
            },
          ),
          drawer: Drawer(
            child: Container(
              color: Colors.green,
            ),
          ),
        ),
        sideBar: SideBar(
          activeIconColor: Colors.green,
          items: [
            MenuItem(
              title: 'Strona główna',
              route: EnumToString.convertToString(SideBarTab.Home, camelCase: true),
              icon: Icons.home,
            ),
            MenuItem(
              title: 'Lista aktywności',
              icon: Icons.list,
              children: [
                MenuItem(
                  title: 'Nowe aktywności',
                  route: EnumToString.convertToString(SideBarTab.List, camelCase: true),
                  icon: Icons.circle,
                ),
              ],
            ),
            MenuItem(
              title: 'Tabela wyników',
              route: EnumToString.convertToString(SideBarTab.Grid, camelCase: true),
              icon: Icons.grid_on,
            ),
          ],
          selectedRoute: _selectedTab,
          onSelected: (MenuItem item) {
            _navigatorBloc.add(nav.ChangeScreenNavigatorEvent(EnumToString.fromString(SideBarTab.values, item.route)));
            setState(() {
              _selectedTab = item.route;
            });
          },
          header: Container(
            height: 50,
            width: double.infinity,
            color: Colors.black26,
            child: Center(
              child: Text(
                'Panel admina',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          footer: Container(
            height: 50,
            width: double.infinity,
            color: Colors.black26,
            child: Center(
              child: Text(
                'version 1.0.0',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

enum SideBarTab {
  Home,
  Grid,
  List,
}
