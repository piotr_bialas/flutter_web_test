import 'package:flutter/material.dart';
import 'package:pluto_grid/pluto_grid.dart';

class GridWidget extends StatefulWidget {
  GridWidget({Key key}) : super(key: key);

  @override
  _GridWidgetState createState() => _GridWidgetState();
}

class _GridWidgetState extends State<GridWidget> {
  @override
  Widget build(BuildContext context) {
    return PlutoGrid(
      columns: [
        PlutoColumn(
          title: "id",
          field: "pole1",
          type: PlutoColumnType.number(),
        ),
        PlutoColumn(
          title: "nazwa",
          field: "pole2",
          type: PlutoColumnType.text(),
        ),
        PlutoColumn(
          title: "cena",
          field: "pole3",
          type: PlutoColumnType.number(format: "#.##"),
        ),
        PlutoColumn(
          title: "data",
          field: "pole4",
          type: PlutoColumnType.date(format: "dd-MM-yyyy"),
        ),
      ],
      rows: [
        PlutoRow(
          cells: {
            "pole1": PlutoCell(value: 1),
            "pole2": PlutoCell(value: "Jabłko"),
            "pole3": PlutoCell(value: 1.49),
            "pole4": PlutoCell(value: DateTime.now().toIso8601String()),
          },
        ),
        PlutoRow(
          cells: {
            "pole1": PlutoCell(value: 2),
            "pole2": PlutoCell(value: "Pomarańcza"),
            "pole3": PlutoCell(value: 2.32),
            "pole4": PlutoCell(value: DateTime.now().toIso8601String()),
          },
        ),
        PlutoRow(
          cells: {
            "pole1": PlutoCell(value: 3),
            "pole2": PlutoCell(value: "Marchewka"),
            "pole3": PlutoCell(value: 0.89),
            "pole4": PlutoCell(value: DateTime.now().toIso8601String()),
          },
        ),
        PlutoRow(
          cells: {
            "pole1": PlutoCell(value: 4),
            "pole2": PlutoCell(value: "Agrest"),
            "pole3": PlutoCell(value: 0.25),
            "pole4": PlutoCell(value: DateTime.now().toIso8601String()),
          },
        ),
        PlutoRow(
          cells: {
            "pole1": PlutoCell(value: 5),
            "pole2": PlutoCell(value: "Arbuz"),
            "pole3": PlutoCell(value: 15.78),
            "pole4": PlutoCell(value: DateTime.now().toIso8601String()),
          },
        ),
      ],
    );
  }
}
