import 'package:flutter/material.dart';
import 'package:pluto_menu_bar/pluto_menu_bar.dart';

class ListWidget extends StatefulWidget {
  ListWidget({Key key}) : super(key: key);

  @override
  _ListWidgetState createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return PlutoMenuBar(
      menus: [
        MenuItem(
          title: "File",
          children: [
            MenuItem(
              title: "New",
              icon: Icons.add,
            ),
            MenuItem(title: "Open"),
            MenuItem(
              title: "Save",
              icon: Icons.save,
            ),
          ],
        ),
        MenuItem(
          title: "Edit",
          children: [
            MenuItem(
              title: "Fast edit",
              children: [
                MenuItem(
                  title: "Very fast edit",
                ),
                MenuItem(
                  title: "Medium fast edit",
                ),
              ]
            ),
            MenuItem(
              title: "Slow edit",
                children: [
                  MenuItem(
                    title: "Very slow edit",
                  ),
                  MenuItem(
                    title: "Medium slow edit",
                  ),
                ]
            ),
          ],
        ),
        MenuItem(
          title: "View",
        ),
        MenuItem(
          title: "Help",
        ),
      ],
    );
  }
}
